This implements the deliverables requested. Verified on API 24 emulator, minversion should be 19

Note: Dynamic grid size was not implemented in the requested timeline, as well as instrumentation test cases and full unit test coverage.