package example.com.tictactoeprj.Dialogs;

import android.os.Bundle;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import example.com.tictactoeprj.GameModel.Utils.ScoreKeepingUtils;
import example.com.tictactoeprj.R;
import example.com.tictactoeprj.databinding.StatsDialogBinding;

/**
 * A dialog for showing the overall win counts/tie count
 */

public class ShowStatsDialog extends AppCompatDialogFragment {
    /**
     * Handler class for UI events (i.e:button clicks, etc)
     */
    public class Handler {

        /**
         * Method to call resetScores() from an UI event
         *
         * @param view the view that triggered the event, not used
         */
        public void onResetClicked(View view) {
            ShowStatsDialog.this.resetScores();
        }
    }

    private Handler mHandler = new Handler();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().setTitle(R.string.game_stats);
        StatsDialogBinding binding = StatsDialogBinding.inflate(inflater, container, false);
        binding.setHandler(mHandler);
        return binding.getRoot();
    }

    /**
     * A method to reset the scores and dismiss the dialog
     */
    public void resetScores() {
        ScoreKeepingUtils.resetScore(getActivity());
        this.dismiss();
    }
}
