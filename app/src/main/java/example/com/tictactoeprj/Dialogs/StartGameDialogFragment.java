package example.com.tictactoeprj.Dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;

import example.com.tictactoeprj.GameModel.Enums.PlayerTurn;
import example.com.tictactoeprj.R;

/**
 * A Dialog to get the user to select a player to go first prior to starting the game
 */

public class StartGameDialogFragment extends AppCompatDialogFragment {
    StartGameDialogSelectedListener mListener;

    /**
     * An interface to pass the result of the selected first turn back to the parent activity
     */
    public interface StartGameDialogSelectedListener {
        void onStartGameDialogSelected(PlayerTurn turn);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // Check to see if the parent implements the listener interface
        try {
            mListener = (StartGameDialogSelectedListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() +
                    " didn't implement StartGameDialogSelectedListener!");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Create a dialog with the 2 player markers as options, to determin who should go first
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle(R.string.starting_game);
        alertDialogBuilder.setMessage(R.string.select_first_player);
        alertDialogBuilder.setPositiveButton(R.string.player_one_marker, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mListener.onStartGameDialogSelected(PlayerTurn.Player1Turn);
            }
        });
        alertDialogBuilder.setNegativeButton(R.string.player_two_marker, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mListener.onStartGameDialogSelected(PlayerTurn.Player2Turn);
            }
        });
        return alertDialogBuilder.create();
    }
}
