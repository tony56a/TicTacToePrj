package example.com.tictactoeprj.GameModel.Enums;

/**
 * An enum representing the result of the game,
 * with none (game in progerss), tie, and win as options
 */

public enum GameResult {
    None,
    Player1Win,
    Player2Win,
    Tie
}
