package example.com.tictactoeprj.GameModel.Enums;

/**
 * An enum representing the current turn of the game
 */

public enum PlayerTurn {
    Player1Turn,
    Player2Turn
}
