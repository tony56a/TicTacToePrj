package example.com.tictactoeprj.GameModel.Enums;

/**
 * An enum representing the state of a square, can be unoccupied, or occupied by either player
 */

public enum SquareState {

    None,
    Player1,
    Player2,
}
