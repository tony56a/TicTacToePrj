package example.com.tictactoeprj.GameModel;

import android.databinding.ObservableArrayList;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;

import java.io.Serializable;

import example.com.tictactoeprj.GameModel.Enums.GameResult;
import example.com.tictactoeprj.GameModel.Enums.PlayerTurn;
import example.com.tictactoeprj.GameModel.Enums.SquareState;

/**
 * Class representing the state of the game, including the state of the game grid,
 * and the result of the game (game in progress/player win/tie)
 */

public class GameModel implements Serializable {

    // Default number of rows and columns for the game grid (usual tic-tac-toe size of 3X3)
    private static final int DEFAULT_NUM_COLUMNS = 3;
    private static final int DEFAULT_NUM_ROWS = 3;

    /**
     * An interface for when a game has completed
     * TODO: Replace with a reactive model perhaps?
     */
    public interface GameCompleteListener {
        /**
         * The method called when the game has completed
         *
         * @param result the result of the game
         */
        void OnGameComplete(GameResult result);
    }

    private transient GameCompleteListener mListener;

    // Number of rows and columns for the current game grid
    private int mNumColumns;
    private int mNumRows;

    /**
     * The state of the game (no game/win/tie)
     */
    private final ObservableField<GameResult> mGameResult = new ObservableField<>();
    /**
     * The current turn in the game
     */
    private final ObservableField<PlayerTurn> mPlayerTurn = new ObservableField<>();
    /**
     * A boolean value representing whether or not a game is currently in progress
     */
    private final ObservableBoolean mIsPlaying = new ObservableBoolean();

    /**
     * An {@link ObservableArrayList} of game square states, representing the current game grid
     */
    private final ObservableArrayList<SquareState> mGameBoardState = new ObservableArrayList<>();

    public ObservableArrayList<SquareState> getGameBoardState() {
        return mGameBoardState;
    }

    public ObservableField<GameResult> getGameResult() {
        return mGameResult;
    }

    public ObservableBoolean getmIsPlaying() {
        return mIsPlaying;
    }

    public ObservableField<PlayerTurn> getmPlayerTurn() {
        return mPlayerTurn;
    }

    public void setmListener(GameCompleteListener mListener) {
        this.mListener = mListener;
    }
    /**
     * Default Constructor, constructs a game model with a 3X3 size game grid
     *
     * @param listener The listener for game completion
     */
    public GameModel(GameCompleteListener listener) {
        this(listener, DEFAULT_NUM_COLUMNS, DEFAULT_NUM_ROWS);
    }

    /**
     * Base Constructor, with the size of of the grid specified by the caller
     *
     * @param listener The listener for game completion
     * @param numColumns number of columns in the game grid
     * @param numRows    number of rows in the game grid
     */
    public GameModel(GameCompleteListener listener, int numColumns, int numRows) {
        //TODO: Only allow 3X3 size grid, since layout is currently hardcoded
        if (numColumns != DEFAULT_NUM_COLUMNS || numRows != DEFAULT_NUM_ROWS) {
            throw new IllegalArgumentException("Variable board size not supported");
        }

        // Make sure a listener is available
        if (listener == null) {
            throw new IllegalArgumentException("Listener must be present");
        }
        this.mListener = listener;

        // Get the grid size, and initialize the game grid
        this.mNumColumns = numColumns;
        this.mNumRows = numRows;
        for (int i = 0; i < this.mNumColumns * this.mNumRows; i++) {
            mGameBoardState.add(i, SquareState.None);
        }

        // Reset game state ( game in progress, current turn, etc )
        this.mIsPlaying.set(false);
        this.mPlayerTurn.set(PlayerTurn.Player1Turn);
        mGameResult.set(GameResult.None);
    }

    /**
     * Update the game state, given the position of the square to be updated.
     * All game states are then updated
     *
     * @param pos the position of the square being updated
     */
    public void makeMove(int pos) {
        SquareState state = this.mGameBoardState.get(pos);
        if (state == SquareState.None) {
            // If the square is not currently occupied, then update the square, and cycle the turn
            // to the next player
            this.mGameBoardState.set(pos, this.mPlayerTurn.get() == PlayerTurn.Player1Turn ? SquareState.Player1 : SquareState.Player2);
            this.mPlayerTurn.set(this.mPlayerTurn.get() == PlayerTurn.Player1Turn ? PlayerTurn.Player2Turn : PlayerTurn.Player1Turn);

            // Check if the game is over, if so, then set the result and unset the game in progress flag
            GameResult result = checkGameState();
            if (result != GameResult.None) {
                mGameResult.set(result);
                this.mIsPlaying.set(false);
                mListener.OnGameComplete(result);
            }
        }
    }

    /**
     * Reset the GameModel state, and set the game to start
     */
    public void startGame(PlayerTurn turn) {
        // Reset game grid
        for (int i = 0; i < this.mNumColumns * this.mNumRows; i++) {
            this.mGameBoardState.set(i, SquareState.None);
        }
        // Set the game in progress value, current first player, and the game result
        this.mIsPlaying.set(true);
        this.mPlayerTurn.set(turn);
        mGameResult.set(GameResult.None);
    }

    /**
     * Checks the game grid to see if there is a winner, or if the game has finished in a tie
     *
     * @return The result of the game if game has concluded, none otherwise
     */
    private GameResult checkGameState() {
        GameResult result;

        // Check horizontal win combinations
        result = checkLineWins(true);
        if (result != GameResult.None) {
            return result;
        }

        // Check vertical win combinations
        result = checkLineWins(false);
        if (result != GameResult.None) {
            return result;
        }

        // Check diagonal win combinations
        result = checkDiagonalWins();
        if (result != GameResult.None) {
            return result;
        }

        // Check if the game has resulted in a tie
        result = checkTie();
        if (result != GameResult.None) {
            return result;
        }

        // If none of the previous checks has resulted in a conclusion,
        // then return none to indicate game should continue
        return GameResult.None;
    }

    /**
     * Checks if the lines have a winning combination in the game grid
     *
     * @param isCheckingHorz a flag to indicate if the horizontal combinations are being checked,
     *                       or the vertical ones are being checked
     * @return The winner if a combination has been found, none otherwise
     */
    private GameResult checkLineWins(boolean isCheckingHorz) {
        int dimenX = isCheckingHorz ? this.mNumRows : this.mNumColumns;
        int dimenY = isCheckingHorz ? this.mNumColumns : this.mNumRows;

        // Iterating over the first dimension, sum up each of the other dimension
        // (i.e: for each row, check all columns in the row/for each column, check all rows)
        // and see if there are enough to make a winning combination for any players
        for (int i = 0; i < dimenX; i++) {
            int player1Marks = 0;
            int player2Marks = 0;
            for (int j = 0; j < dimenY; j++) {
                // Compute the index, based on whether or not we're checking horizontal combinations,
                // or vertical ones
                int index = isCheckingHorz ? (i * dimenX + j) : (j * dimenY + i);
                SquareState squareState = this.mGameBoardState.get(index);
                if (squareState == SquareState.Player1) {
                    player1Marks++;
                } else if (squareState == SquareState.Player2) {
                    player2Marks++;
                }
            }

            // If there's a winning combination, return the winner with the combination
            if (player1Marks == dimenX) {
                return GameResult.Player1Win;
            } else if (player2Marks == dimenX) {
                return GameResult.Player2Win;
            }

        }

        // Otherwise, continue with the game
        return GameResult.None;
    }

    private GameResult checkDiagonalWins() {
        int player1Marks = 0;
        int player2Marks = 0;

        // Check the top left to bottom right combination
        for (int i = 0; i < this.mNumRows * this.mNumColumns; i += (this.mNumColumns + 1)) {
            SquareState squareState = this.mGameBoardState.get(i);
            if (squareState == SquareState.Player1) {
                player1Marks++;
            } else if (squareState == SquareState.Player2) {
                player2Marks++;
            }
        }

        // If a winning combination, return the winner
        if (player1Marks == this.mNumRows) {
            return GameResult.Player1Win;
        } else if (player2Marks == this.mNumRows) {
            return GameResult.Player2Win;
        }

        player1Marks = 0;
        player2Marks = 0;

        // Check the top right to bottom left combination
        for (int i = this.mNumRows - 1; i < ((this.mNumRows * (this.mNumColumns - 1)) + 1); i += (this.mNumColumns - 1)) {
            SquareState squareState = this.mGameBoardState.get(i);
            if (squareState == SquareState.Player1) {
                player1Marks++;
            } else if (squareState == SquareState.Player2) {
                player2Marks++;
            }
        }

        // If a winning combination, return the winner
        if (player1Marks == this.mNumRows) {
            return GameResult.Player1Win;
        } else if (player2Marks == this.mNumRows) {
            return GameResult.Player2Win;
        }

        // Otherwise, continue with the game
        return GameResult.None;
    }


    /**
     * Function to check if all squares in the grid have been occupied, resulting in a tie
     *
     * @return a Tie if all squares have been occupied, none otherwise
     */
    private GameResult checkTie() {

        // Iterate over all squares, return none if any are empty
        for (int i = 0; i < this.mNumRows * this.mNumColumns; i++) {
            if (this.mGameBoardState.get(i) == SquareState.None) {
                return GameResult.None;
            }
        }

        // Otherwise, all squares are occupied, return tie in that case
        return GameResult.Tie;
    }

}
