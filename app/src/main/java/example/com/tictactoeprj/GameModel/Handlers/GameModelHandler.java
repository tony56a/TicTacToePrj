package example.com.tictactoeprj.GameModel.Handlers;

import example.com.tictactoeprj.GameModel.Enums.PlayerTurn;
import example.com.tictactoeprj.GameModel.GameModel;

/**
 * Class for handlers for UI events for the Game Model ( button presses, etc )
 */

public class GameModelHandler {

    GameModel mGameModel;

    public GameModelHandler(GameModel gameModel) {
        this.mGameModel = gameModel;
    }

    /**
     * Calls the makeMove method to update the game model with the position clicked
     *
     * @param squareIndex the index of the clicked square
     */
    public void setSquare(int squareIndex) {
        mGameModel.makeMove(squareIndex);
    }

    /**
     * Calls the startGame method in the game model to set the state to start the game
     * @param turn The player for the first turn
     */
    public void startGame(PlayerTurn turn) {
        mGameModel.startGame(turn);
    }

}
