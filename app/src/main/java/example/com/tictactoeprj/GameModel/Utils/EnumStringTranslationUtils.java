package example.com.tictactoeprj.GameModel.Utils;

import android.content.Context;

import example.com.tictactoeprj.GameModel.Enums.GameResult;
import example.com.tictactoeprj.GameModel.Enums.PlayerTurn;
import example.com.tictactoeprj.GameModel.Enums.SquareState;
import example.com.tictactoeprj.R;

/**
 * An utility class that translates the enum state into
 * a string representation for display in the binding
 */

public class EnumStringTranslationUtils {

    /**
     * A method to translate the player state into a symbol used for display to the user
     * TODO: Update to allow for Drawables to be returned as well
     *
     * @param state   The square state to be translated
     * @param context A context, required to get the resource strings
     * @return The string used to represent the player
     */
    public static String getPlayerSymbol(SquareState state, Context context) {
        switch (state) {
            // If none, make sure it's an empty string
            case None:
                return " ";
            // Otherwise, return the representation for the player 1 and 2 marker values
            case Player1:
                return context.getApplicationContext().getResources().getString(R.string.player_one_marker);
            case Player2:
                return context.getApplicationContext().getResources().getString(R.string.player_two_marker);
            // Default value, should not reach here
            default:
                return " ";
        }
    }

    /**
     * A method to translate the game result into a string used for display to the user
     *
     * @param result    The result of the game
     * @param isPlaying Whether or not a game is in progress
     * @param context   A context, required to get the resource strings
     * @return The string used to represent the game result
     */
    public static String getGameResult(GameResult result, boolean isPlaying, Context context) {
        // If game in progress, indicate so to user
        if (isPlaying) {
            return context.getApplicationContext().getResources().getString(R.string.currently_playing);
        } else {
            switch (result) {
                // If none, indicate that no game is in progress
                case None:
                    return context.getApplicationContext().getResources().getString(R.string.not_currently_playing);
                // Otherwise, return the representation for the game conclusion
                case Player1Win:
                    return context.getApplicationContext().getResources().getString(R.string.player_one_wins);
                case Player2Win:
                    return context.getApplicationContext().getResources().getString(R.string.player_two_wins);
                case Tie:
                    return context.getApplicationContext().getResources().getString(R.string.tie);
                // Default value, should not reach here
                default:
                    return " ";
            }
        }
    }

    /**
     * A method to translate the current turn into a string used for display to the user
     *
     * @param turn      the turn enum to be translated
     * @param isPlaying Whether or not a game is in progress
     * @param context   A context, required to get the resource strings
     * @return The string used to represent the turn
     */
    public static String getCurrentTurn(PlayerTurn turn, boolean isPlaying, Context context) {
        if (!isPlaying) {
            // If not playing, don't show anything
            return " ";
        } else {
            switch (turn) {
                // Return the representation for the turn
                case Player1Turn:
                    return context.getApplicationContext().getResources().getString(R.string.player_one_turn);
                case Player2Turn:
                    return context.getApplicationContext().getResources().getString(R.string.player_two_turn);
                // Default value, should not reach here
                default:
                    return " ";
            }
        }
    }
}
