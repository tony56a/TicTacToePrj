package example.com.tictactoeprj.GameModel.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import example.com.tictactoeprj.GameModel.Enums.GameResult;

/**
 * An utility class to store the global stats for each game using SharedPreferences
 */

public class ScoreKeepingUtils {

    public static final String SHARED_PREF_KEY = "tictactoe";
    public static final String PLAYER_ONE_KEY = "player1";
    public static final String PLAYER_TWO_KEY = "player2";
    public static final String TIED_KEY = "tied";

    /**
     * A utility method to get the appropriate key, given the GameResult value
     *
     * @param result the GameResult to get the key for
     * @return the key to the SharedPreferences for the GameResult if valid, null otherwise
     */
    private static String resultEnumToKey(GameResult result) {
        switch (result) {
            case Player1Win:
                return PLAYER_ONE_KEY;
            case Player2Win:
                return PLAYER_TWO_KEY;
            case Tie:
                return TIED_KEY;
            default:
                return null;
        }
    }

    /**
     * A method to directly set the score for a player
     *
     * @param result  The GameResult to set the score for
     * @param score   The new score
     * @param context A context to get the SharedPreferences
     */
    public static void setScore(GameResult result, int score, Context context) {
        SharedPreferences sharedPrefs = context.getApplicationContext().getSharedPreferences(SHARED_PREF_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPrefs.edit();
        String key = resultEnumToKey(result);
        if (key != null) {
            editor.putInt(key, score);
        }
        editor.commit();
    }

    /**
     * A method to increment the score for a player
     *
     * @param result  The GameResult to increment the score for
     * @param context A context to get the SharedPreferences
     */
    public static void incrementScore(GameResult result, Context context) {
        SharedPreferences sharedPrefs = context.getApplicationContext().getSharedPreferences(SHARED_PREF_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPrefs.edit();
        String key = resultEnumToKey(result);
        if (key != null) {
            int playerWins = sharedPrefs.getInt(key, 0);
            editor.putInt(key, playerWins + 1);
            editor.commit();
        }

    }

    /**
     * A method to reset the score for all players
     *
     * @param context A context to get the SharedPreferences
     */
    public static void resetScore(Context context) {
        SharedPreferences sharedPrefs = context.getApplicationContext().getSharedPreferences(SHARED_PREF_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putInt(PLAYER_ONE_KEY, 0);
        editor.putInt(PLAYER_TWO_KEY, 0);
        editor.putInt(TIED_KEY, 0);

        editor.commit();
    }

    /**
     * A method to get the score for a player
     *
     * @param result  The GameResult to get the score for
     * @param context A context to get the SharedPreferences
     * @return the score for the GameResult if applicable, 0 otherwise
     */
    public static int getScore(GameResult result, Context context) {
        SharedPreferences sharedPrefs = context.getApplicationContext().getSharedPreferences(SHARED_PREF_KEY, Context.MODE_PRIVATE);
        String key = resultEnumToKey(result);
        int playerWins = 0;
        if (key != null) {
            playerWins = sharedPrefs.getInt(key, 0);
        }
        return playerWins;
    }
}
