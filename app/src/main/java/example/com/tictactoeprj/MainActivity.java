package example.com.tictactoeprj;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import example.com.tictactoeprj.Dialogs.ShowStatsDialog;
import example.com.tictactoeprj.Dialogs.StartGameDialogFragment;
import example.com.tictactoeprj.GameModel.Enums.GameResult;
import example.com.tictactoeprj.GameModel.Enums.PlayerTurn;
import example.com.tictactoeprj.GameModel.GameModel;
import example.com.tictactoeprj.GameModel.Handlers.GameModelHandler;
import example.com.tictactoeprj.GameModel.Utils.ScoreKeepingUtils;
import example.com.tictactoeprj.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity implements StartGameDialogFragment.StartGameDialogSelectedListener, GameModel.GameCompleteListener {

    public static final String GAME_STATE_KEY = "gameState";

    GameModel mGameModel;
    GameModelHandler mModelHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Load the model from the savedInstanceState if available
        if (savedInstanceState != null && savedInstanceState.getSerializable(GAME_STATE_KEY) != null) {
            mGameModel = (GameModel) savedInstanceState.getSerializable(GAME_STATE_KEY);
            mGameModel.setmListener(this);
        } else {
            mGameModel = new GameModel(this);
        }
        mModelHandler = new GameModelHandler(mGameModel);
        // Bind layout to game model
        ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.setGameModel(mGameModel);
        binding.setHandler(mModelHandler);
    }

    @Override
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putSerializable(GAME_STATE_KEY, mGameModel);
        super.onSaveInstanceState(bundle);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        FragmentManager manager = getSupportFragmentManager();

        switch (item.getItemId()) {
            case R.id.startGame:
                // Start the start game dialog fragment
                StartGameDialogFragment startGameDialog = new StartGameDialogFragment();
                startGameDialog.show(manager, null);
                break;
            case R.id.getStats:
                ShowStatsDialog statsDialog = new ShowStatsDialog();
                statsDialog.show(manager, null);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStartGameDialogSelected(PlayerTurn turn) {
        // Start the game with the given first turn player
        mModelHandler.startGame(turn);
    }

    @Override
    public void OnGameComplete(GameResult result) {
        // Update the score
        ScoreKeepingUtils.incrementScore(result, this);
    }
}
