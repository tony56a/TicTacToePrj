package example.com.tictactoeprj;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import example.com.tictactoeprj.GameModel.Enums.GameResult;
import example.com.tictactoeprj.GameModel.Enums.PlayerTurn;
import example.com.tictactoeprj.GameModel.Enums.SquareState;
import example.com.tictactoeprj.GameModel.GameModel;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

/**
 * Created by applecobbler on 12/5/2016.
 */

public class GameModelTest {

    public static final int EXPECTED_NUM_COLUMNS = 3;
    public static final int EXPECTED_NUM_ROWS = 3;

    Map<String, Integer> statistics = new HashMap<>();

    GameModel.GameCompleteListener gameCompleteListener;

    /**
     * Helper method to run a full game with the gamemodel
     *
     * @param model          the GameModel under test
     * @param moves          the sequence of moves to make
     * @param firstTurn      the first turn player
     * @param expectedTurn   the expected player turn once the last move is made
     * @param expectedResult the expected result of the game
     */
    private void playGame(GameModel model, int[] moves, PlayerTurn firstTurn,
                          PlayerTurn expectedTurn, GameResult expectedResult) {
        PlayerTurn currentTurn = firstTurn;
        // Get a set of unmade moves
        HashSet<Integer> unmadeMoves = new HashSet<>();
        Collections.addAll(unmadeMoves, 0, 1, 2, 3, 4, 5, 6, 7, 8);
        for (int move : moves) {
            unmadeMoves.remove(move);
        }


        model.startGame(currentTurn);

        // make each move, and verify the state has been updated
        for (int moveToMake : moves) {
            model.makeMove(moveToMake);
            SquareState state = currentTurn == PlayerTurn.Player1Turn ? SquareState.Player1 : SquareState.Player2;
            assertEquals(model.getGameBoardState().get(moveToMake), state);
            currentTurn = currentTurn == PlayerTurn.Player1Turn ? PlayerTurn.Player2Turn : PlayerTurn.Player1Turn;
        }

        // Verify the final state is correct
        assertEquals(model.getGameResult().get(), expectedResult);
        assertEquals(model.getmPlayerTurn().get(), expectedTurn);
        assertEquals(currentTurn, expectedTurn);

        // Verify unmade moves are still none
        for (int unmadeMove : unmadeMoves) {
            assertEquals(model.getGameBoardState().get(unmadeMove), SquareState.None);
        }
    }

    @Before
    public void before() {
        gameCompleteListener = Mockito.mock(GameModel.GameCompleteListener.class);
    }

    /**
     * Test to verify that the game starting updates the state correctly
     */
    @Test
    public void testStartGame() {
        GameModel model = new GameModel(gameCompleteListener);
        // Verify that the state before the game has started is correct
        assertEquals(model.getmPlayerTurn().get(), PlayerTurn.Player1Turn);
        assertFalse(model.getmIsPlaying().get());
        assertEquals(model.getGameResult().get(), GameResult.None);
        assertEquals(model.getGameBoardState().size(), EXPECTED_NUM_COLUMNS * EXPECTED_NUM_ROWS);
        for (int i = 0; i < model.getGameBoardState().size(); i++) {
            assertEquals(model.getGameBoardState().get(i), SquareState.None);
        }

        model.startGame(PlayerTurn.Player2Turn);
        assertEquals(model.getmPlayerTurn().get(), PlayerTurn.Player2Turn);
        assertTrue(model.getmIsPlaying().get());
        assertEquals(model.getGameResult().get(), GameResult.None);
        for (int i = 0; i < model.getGameBoardState().size(); i++) {
            assertEquals(model.getGameBoardState().get(i), SquareState.None);
        }
    }

    /**
     * Test to verify that the setSquare() method works correctly
     */
    @Test
    public void testSetSquare() {
        int moveToMake = 1;
        GameModel model = new GameModel(gameCompleteListener);
        model.startGame(PlayerTurn.Player1Turn);
        model.makeMove(moveToMake);

        assertEquals(model.getmPlayerTurn().get(), PlayerTurn.Player2Turn);
        for (int i = 0; i < model.getGameBoardState().size(); i++) {
            assertEquals(model.getGameBoardState().get(i), i == moveToMake ? SquareState.Player1 : SquareState.None);
        }
    }

    /**
     * Test to verify that the horizontal win conditions are valid
     */
    @Test
    public void testHorizontalWin() {
        int[] movesToMake = {0, 3, 1, 4, 2};
        GameModel model = new GameModel(gameCompleteListener);
        playGame(model, movesToMake, PlayerTurn.Player1Turn, PlayerTurn.Player2Turn, GameResult.Player1Win);
        playGame(model, movesToMake, PlayerTurn.Player2Turn, PlayerTurn.Player1Turn, GameResult.Player2Win);

    }

    /**
     * Test to verify that the vertical win conditions are valid
     */
    @Test
    public void testVerticalWin() {
        int[] movesToMake = {0, 1, 3, 4, 6};

        GameModel model = new GameModel(gameCompleteListener);
        playGame(model, movesToMake, PlayerTurn.Player1Turn, PlayerTurn.Player2Turn, GameResult.Player1Win);
        playGame(model, movesToMake, PlayerTurn.Player2Turn, PlayerTurn.Player1Turn, GameResult.Player2Win);
    }

    /**
     * Test to verify that the diagonal win conditions are valid
     */
    @Test
    public void testDiagonalWin() {
        int[] topLeftToBotomRight = {0, 1, 4, 5, 8};
        int[] topRightToBottomLeft = {2, 1, 4, 3, 6};

        GameModel model = new GameModel(gameCompleteListener);
        playGame(model, topLeftToBotomRight, PlayerTurn.Player1Turn, PlayerTurn.Player2Turn, GameResult.Player1Win);
        playGame(model, topLeftToBotomRight, PlayerTurn.Player2Turn, PlayerTurn.Player1Turn, GameResult.Player2Win);

        playGame(model, topRightToBottomLeft, PlayerTurn.Player1Turn, PlayerTurn.Player2Turn, GameResult.Player1Win);
        playGame(model, topRightToBottomLeft, PlayerTurn.Player2Turn, PlayerTurn.Player1Turn, GameResult.Player2Win);
    }

    /**
     * Test to verify that the tie conditions are valid
     */
    @Test
    public void testTie() {
        int[] movesToMake = {0, 1, 2, 3, 5, 4, 7, 8, 6};

        GameModel model = new GameModel(gameCompleteListener);
        playGame(model, movesToMake, PlayerTurn.Player2Turn, PlayerTurn.Player1Turn, GameResult.Tie);
        playGame(model, movesToMake, PlayerTurn.Player1Turn, PlayerTurn.Player2Turn, GameResult.Tie);

    }
}
